<?php

use App\Model\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['user_id'=>'1', 'title'=>'Games', 'content'=>'dddddddddddddddddddddddddddddddddd'],
            ['user_id'=>'1', 'title'=>'Games', 'content'=>'dddddddddddddddddddddddddddddddddd'],
            ['user_id'=>'1', 'title'=>'Games', 'content'=>'dddddddddddddddddddddddddddddddddd'],
            ['user_id'=>'1', 'title'=>'Games', 'content'=>'dddddddddddddddddddddddddddddddddd'],
            ['user_id'=>'1', 'title'=>'Games', 'content'=>'dddddddddddddddddddddddddddddddddd'],
            ['user_id'=>'1', 'title'=>'Games', 'content'=>'dddddddddddddddddddddddddddddddddd'],
        ];

        foreach ($data as $record) {
            
            Post::create(
                $record    
            );
        }
    }
}
